# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!
AlbumToYoutube::Application.default_url_options = AlbumToYoutube::Application.config.action_mailer.default_url_options
