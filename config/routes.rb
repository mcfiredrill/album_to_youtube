Rails.application.routes.draw do
  mount_ember_assets :frontend, to: "/"
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #

  resources :uploader_signature, only: [:index]
  resources :uploads, only: [:create]
  resources :tracks, only: [:create]
  resources :images, only: [:create]
  root 'home#index'
end
