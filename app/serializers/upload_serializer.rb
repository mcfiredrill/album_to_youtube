class UploadSerializer < ActiveModel::Serializer
  attributes :id, :title, :description
end
