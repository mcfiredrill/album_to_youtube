class ImageSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attributes :id, :layer_order, :file, :file_url

  def file_url
    rails_blob_path(object.file)
  end
end
