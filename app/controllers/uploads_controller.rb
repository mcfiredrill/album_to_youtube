class UploadsController < ApplicationController
  def create
    upload = current_user.uploads.new

    if upload.save
      render json: upload
    else
    end
  end
end
