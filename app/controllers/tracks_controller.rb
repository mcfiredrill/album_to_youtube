class TracksController < ApplicationController
  def create
    track = Track.new track_params
    if track.save
      render json: track
    else
    end
  end

  private
  def track_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params,
                                                          only: ["audio-file-name", :upload_id, :upload])
  end
end
