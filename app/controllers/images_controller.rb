class ImagesController < ApplicationController
  def create
    image = Image.new image_params
    if image.save
      render json: image
    else
    end
  end

  private
  def image_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:upload_id, :upload, :file])
  end
end
