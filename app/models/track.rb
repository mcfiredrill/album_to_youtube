class Track < ApplicationRecord
  include SoId3::BackgroundJobs
  belongs_to :upload, dependent: :destroy

  has_tags column: :s3_filepath, storage: :s3,
           artwork_column: :artwork,
           s3_credentials: { bucket: ENV['S3_BUCKET'],
                             access_key_id: ENV['S3_KEY'],
                             secret_access_key: ENV['S3_SECRET'],
                             region: ENV['S3_REGION'] }

  enum tag_processing_status: ['unprocessed', 'processing', 'done', 'failed']
end
