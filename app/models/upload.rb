class Upload < ApplicationRecord
  has_one :track
  belongs_to :user
  has_many :images
end
