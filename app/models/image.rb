class Image < ApplicationRecord
  belongs_to :upload
  has_one_attached :file
  before_create :set_layer_order

  private

  def set_layer_order
    order = (upload.images.pluck(:layer_order).max || 0) + 1
    self.layer_order = order
  end
end
