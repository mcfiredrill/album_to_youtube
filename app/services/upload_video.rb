class UploadVideo
  def self.upload upload
    user = upload.user
    account = Yt::Account.new access_token: user.api_token
    account.upload_video
  end
end
