FactoryBot.define do
  factory :user do
    email 'yoshino@malboro.info'
    password "password"
    password_confirmation "password"
  end
end
