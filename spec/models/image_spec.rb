require 'rails_helper'

RSpec.describe Image, type: :model do
  it "sets the layer order" do
    user = FactoryBot.create :user
    upload = Upload.create user: user
    image = Image.create upload: upload
    expect(image.layer_order).to eq 1
    image = Image.create upload: upload
    expect(image.layer_order).to eq 2
  end
end
