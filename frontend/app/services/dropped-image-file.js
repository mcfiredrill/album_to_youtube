import Service from '@ember/service';

export default Service.extend(Ember.Evented, {
  sendDroppedFile: function(file){
    console.log(file);
    this.trigger('fileWasDropped', file);
  }
});
