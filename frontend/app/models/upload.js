import DS from 'ember-data';
import { sort } from '@ember/object/computed';
import { filter } from '@ember/object/computed';
import { computed } from '@ember/object';

export default DS.Model.extend({
  userId: DS.attr(),
  tracks: DS.hasMany('track'),
  images: DS.hasMany('image'),
  title: DS.attr(),
  description: DS.attr(),
  imageSortKeys: ['layerOrder:asc'],
  _sortedImages: sort('images', 'imageSortKeys'),
  sortedImages: filter('_sortedImages.@each.isNew', function(image){
    return !image.get('isNew');
  }),
  baseImage: computed('sortedImages.[]', function(){
    console.log('baseImage');
    return this.get('sortedImages.firstObject');
  })
});
