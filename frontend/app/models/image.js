import DS from 'ember-data';

export default DS.Model.extend({
  upload: DS.belongsTo('upload'),
  layerOrder: DS.attr(),
  file: DS.attr(),
  fileUrl: DS.attr(),
  fileType: DS.attr(),
  // gif stuff
  frames: [], // array of frames
  currentFrame: 0
});
