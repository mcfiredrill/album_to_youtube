import DS from 'ember-data';

export default DS.Model.extend({
  upload: DS.belongsTo('upload'),
  isUploading: false,
  audioFileName: DS.attr(),
  uploadProgress: 0,
  filesize: DS.attr(),
});
