import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    setUpload(upload){
      console.log('setting upload');
      this.set('upload', upload);
    },
    saveImage(signedId){
      let image;
      const finishSavingImage = (upload) => {
        image = this.get('store').createRecord('image', { upload: upload });
        image.set('file', signedId);
        image.save().then(() => {
          upload.get('images').pushObject(image);
          this.set('upload', upload);
        });
      };
      if(!this.get('upload')){
        const upload = this.store.createRecord('upload');
        upload.save().then(() => {
          finishSavingImage(upload);
        });
      }else{
        const upload = this.get('upload');
        finishSavingImage(upload);
      }
    }
  }
});
