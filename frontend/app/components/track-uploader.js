import FileField from 'ember-uploader/components/file-field';
import S3Uploader from 'ember-uploader/uploaders/s3';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';

export default FileField.extend({
  //droppedFile: service(),
  signingUrl: '/uploader_signature',
  store: service(),

  init() {
    this._super(...arguments);
    this.set('validMimeTypes', ["audio/mp3", "audio/mpeg"]);
    // this.get('droppedFile').on('fileWasDropped', e => {
    //   this.filesDidChange(e);
    // });
  },
  filesDidChange(files) {
    if (!isEmpty(files)) {
      for(let i = 0; i< files.length; i++){
        console.log(files[i].type);
        if(!this.validMimeTypes.includes(files[i].type)){
          console.log("invalid mime type: " + files[i].type);
          //Ember.get(this, 'flashMessages').danger("Sorry, there was an error uploading this file. This doesn't appear to be a valid audio file.");
          continue;
        }

        let mimeType;
        if(files[i].type == "audio/mp3"){
          mimeType = "audio/mpeg";
        }else{
          mimeType = files[i].type;
        }

        console.log(mimeType);

        let uploader = S3Uploader.create({
          signingUrl: this.get('signingUrl'),
          method: "PUT",
          ajaxSettings: {
            headers: {
              'Content-Type': mimeType,
              'x-amz-acl': 'public-read'
            }
          }
        });
        const store = this.get('store');
        this.set('track', store.createRecord('track', {
          isUploading: true,
          filesize: files[i].size,
          audioFileName: files[i].name}));
        this.set('upload', store.createRecord('upload'));
        window.onbeforeunload = function(e) {
          const dialogText = "You are currently uploading files. Closing this tab will cancel the upload operation! Are you usure you want to close this tab?";
          e.returnValue = dialogText;
          return dialogText;
        };

        uploader.on('didSign', (response) => {
          this.set('finalFileName', response.endpoint.split("?")[0]);
        });

        uploader.on('didUpload', () => {
          window.onbeforeunload = null;
          let track = this.get('track');
          track.set('audioFileName', this.get('finalFileName'));
          track.set('isUploading', false);
          let onSuccess = () =>{
            console.log("track saved!");
            this.get('setUpload')(this.get("upload"));
          };
          let onFail = () => {
            console.log("track save failed");
            //Ember.get(this, 'flashMessages').danger("Sorry, something went wrong uploading this file!");
          };
          let upload = this.get('upload');
          upload.save().then( () => {
            track.set("upload", upload);
            track.save().then(onSuccess, onFail);
          });
        });

        uploader.on('progress', (e) => {
          this.set("track.uploadProgress", e.percent);
        });

        uploader.on('didError', (jqXHR, textStatus, errorThrown) => {
          window.onbeforeunload = null;
          //Ember.get(this, 'flashMessages').danger("Sorry, something went wrong!");
          console.log("ERROR!" + textStatus);
          console.log("ERROR!" + errorThrown);
        });

        uploader.upload(files[i]);
      }
    }
  }
});
