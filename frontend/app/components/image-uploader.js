import FileField from 'ember-uploader/components/file-field';
import { inject as service } from '@ember/service';
import { get, set } from '@ember/object';

export default FileField.extend({
  activeStorage: service(),
  store: service(),
  droppedImageFile: service(),
  uploadProgress: 0,

  init() {
    this._super(...arguments);
    this.get('droppedImageFile').on('fileWasDropped', e => {
      this.filesDidChange(e);
    });
  },

  filesDidChange(files) {
    if (files) {
      const directUploadURL = '/rails/active_storage/direct_uploads';
      for (let i = 0; i < files.length; i++) {
        get(this, 'activeStorage').upload(files.item(i), directUploadURL, {
          onProgress: (progress) => {
            set(this, 'uploadProgress', progress);
          }
        }).then( (blob) => {
          const signedId = get(blob, 'signedId');
          this.get('saveImage')(signedId);
        });
      }
    }
  }
});
