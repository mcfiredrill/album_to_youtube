import Component from '@ember/component';
import { next } from '@ember/runloop';
import { later } from '@ember/runloop';
import { inject as service } from '@ember/service';
import { observer } from '@ember/object';
import { filter } from '@ember/object/computed';
import { isEmpty } from '@ember/utils';

import { GIF } from 'gifuct-js';

export default Component.extend({
  classNames        : [ 'draggableDropzone' ],
  classNameBindings : [ 'dragClass' ],
  dragClass         : 'deactivated',

  droppedImageFile: service(),
  frameIndex: 0,

  dragLeave(event) {
    event.preventDefault();
    //[set(this, 'dragClass', 'deactivated');
    //Ember.$(".uploader-icon").hide();
  },

  dragOver(event) {
    event.preventDefault();
    //[set(this, 'dragClass', 'activated');
    //Ember.$(".uploader-icon").show();
  },
  drop(event){
    console.log('file dropped');
    this.get('droppedImageFile').sendDroppedFile(event.dataTransfer.files);
    event.preventDefault();
  },

  setBaseWidthAndHeight: observer('baseImage.fileUrl', function(){
    const img = new Image();
    img.onload = () => {
      this.set('baseImageWidth', img.width);
      this.set('baseImageHeight', img.height);
    };
    img.src = this.get('baseImage.fileUrl');
  }),

  _updateGifFrames(){
    let frameIndex = this.get('frameIndex');
    this._gifs.forEach((gif) => {
      const frameCount = gif.get('frames').length;
      gif.set('currentFrame', frameIndex % frameCount);
    });
    this.set('frameIndex', frameIndex+1);

    this._redrawCanvas();

    later( () => {
      this._updateGifFrames();
    }, 500);
  },

  _gifs: filter('images.@each.fileType', function(image){
    return image.get('fileType') === 'image/gif';
  }),

  _parseGifs: observer('_gifs.[]', function(){
    this.get('_gifs').forEach((image) => {
      const oReq = new XMLHttpRequest();
      oReq.open("GET", image.get('fileUrl'), true);
      oReq.responseType = "arraybuffer";

      oReq.onload = function (oEvent) {
        const arrayBuffer = oReq.response; // Note: not oReq.responseText
        if (arrayBuffer) {
          const gif = new window.GIF(arrayBuffer);
          const frames = gif.decompressFrames(true);
          image.set('frames', frames);
          //render the gif
          //renderGIF(frames);
        }
      };

      oReq.send(null);
      //
    });
  }),

  init(){
    this._super(...arguments)
    later( () => {
      this._updateGifFrames();
    }, 40);
  },

  didRender(){
    this._redrawCanvas();
  },

  _redrawCanvas(){
    if(this.get('baseImage')){
      next( () => {
        const canvas = document.getElementById("canvas");
        const ctx = canvas.getContext("2d");

        // clear the canvas
        ctx.clearRect(0,0,canvas.width,canvas.height);

        this.get('images').forEach((image) => {
          const img = new Image();

          const imageX=0;
          const imageY=0;
          let imageWidth,imageHeight; //,imageRight,imageBottom;

          if(image.get('fileType') === "image/gif" && !isEmpty(image.get('frames'))){
            //play current frame
            const frame = image.get('frames')[image.get('currentFrame')];
            const tempCanvas = document.createElement('canvas');
            const tempCtx = tempCanvas.getContext('2d');
            const dims = frame.dims;
            tempCanvas.width = dims.width;
            tempCanvas.height = dims.height;

            const frameImageData = tempCtx.createImageData(dims.width, dims.height);
            // set the patch data as an override
            frameImageData.data.set(frame.patch);
            tempCtx.putImageData(frameImageData, 0, 0);
            //ctx.drawImage(tempCanvas,0,0,dims.width,dims.height,imageX,imageY,dims.width,dims.height);
            ctx.drawImage(tempCanvas,0,0);
          }else{

            img.onload = () => {
              imageWidth=img.width;
              imageHeight=img.height;
              // imageRight=imageX+imageWidth;
              // imageBottom=imageY+imageHeight
              //
              // this.set('baseImageWidth', imageWidth);
              // this.set('baseImageHeight', imageHeight);

              // draw the image
              ctx.drawImage(img,0,0,img.width,img.height,imageX,imageY,imageWidth,imageHeight);
            };
            //debugger;
            img.src = image.get('fileUrl');
          }
        });
      });
    }
  }
});
