import EmberObject from '@ember/object';
import CanvasMixin from 'frontend/mixins/canvas';
import { module, test } from 'qunit';

module('Unit | Mixin | canvas', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let CanvasObject = EmberObject.extend(CanvasMixin);
    let subject = CanvasObject.create();
    assert.ok(subject);
  });
});
