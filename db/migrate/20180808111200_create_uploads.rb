class CreateUploads < ActiveRecord::Migration[5.2]
  def change
    create_table :uploads do |t|
      t.references :user, null: false, index: true
      t.integer :status, null: false, default: 0

      t.timestamps
    end
  end
end
