class AddLayerOrderToImages < ActiveRecord::Migration[5.2]
  def change
    add_column :images, :layer_order, :integer, null: false, default: 0
  end
end
