class CreateTracks < ActiveRecord::Migration[5.2]
  def change
    create_table :tracks do |t|
      t.string :audio_file_name
      t.references :upload, null: false, index: true

      t.timestamps
    end
  end
end
