class AddTitleDescriptionToUploads < ActiveRecord::Migration[5.2]
  def change
    add_column :uploads, :title, :string
    add_column :uploads, :description, :string
  end
end
